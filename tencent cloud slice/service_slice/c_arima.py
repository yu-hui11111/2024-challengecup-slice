"""
ARIMA带宽预测算法（旧版） 默认差分一次
考虑使用仿真环境下跑通的新库改改？(doing)
"""

import warnings
import numpy as np
import pandas as pd
from statsmodels.tsa.arima.model import ARIMA

# data是历史带宽数据，length是预测未来时间长度
def forecast(data, length):
    # print('-'*20, 'IGNORE THIS WARNING', '-'*20)
    data = [round(x/1024, 4) for x in data]
    if len(data) == 0:
        data = [3.1, 3.2, 3.1, 3.2, 3.1, 3.2, 3.4, 3.5, 3.2, 3.3]
        length = len(data) - 1
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        model = ARIMA(data, order=(1, 0, 1))
        res = model.fit()
        answer = res.forecast(length) # （）内的是预测数量
    return answer

    # TM[i][j] = last_TM[i][j] + result




if __name__ == "__main__":
    print(forecast([3.1, 3.2, 3.1, 3.2, 3.1, 3.2, 3.4, 3.5, 3.2, 3.3],3))