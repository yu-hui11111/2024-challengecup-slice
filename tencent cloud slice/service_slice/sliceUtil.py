'''
工具类，用于解析终端传递的msg字符串
将list转化为str
等
'''
import pltres

# 解析终端发送的数据
def parsemsg(msg, tp):
    if tp == 0:
        band = getarray(msg, 'BandWidth=[')
        pack = getarray(msg, 'PacketLoss=[')
        delay = getarray(msg, 'Delay=[')
        jitter = getarray(msg, 'Jitter=[')
        jitter.append(0) # 长度为119的补充最后一位
        return band, pack, delay, jitter
    elif tp == 1:
        idx = msg.find('req=')
        if idx != -1:
            req = int(msg[idx + 4]) # 0-3
        else:
            req = -1
        
        idx = msg.find('UpBandWidth=')
        if idx != -1 :
            idx2 = msg.find(',', idx)
            ubw = int(msg[idx + len('UpBandWidth='):idx2])
        else:
            ubw = 0

        idx = msg.find('DownBandWidth=')
        if idx != -1 :
            idx2 = msg.find('}', idx)
            dbw = int(msg[idx + len('DownBandWidth='):idx2])
        else:
            dbw = 0

        idx = msg.find('dbm=')
        if idx != -1 :
            idx2 = msg.find(',', idx)
            dbm = int(msg[idx + len('dbm='):idx2])
        else:
            dbm = 0
        
        idx = msg.find('Rsrq=')
        if idx != -1 :
            idx2 = msg.find(',', idx)
            Rsrq = int(msg[idx + len('Rsrq='):idx2])
        else:
            Rsrq = 0

        idx = msg.find('port:')
        if idx != -1:
            idx2 = msg.find(', END', idx)
            port = int(msg[idx+5:idx2])
        else:
            port = ''
        band = getarray(msg, 'BandWidth=[')
        pack = getarray(msg, 'PacketLoss=[')
        delay = getarray(msg, 'Delay=[')
        jitter = getarray(msg, 'Jitter=[')
        jitter.append(0) # jitter补充最后一位

        return req, ubw, dbw, dbm, Rsrq, band, pack, delay, jitter, port

# 解析终端发送的网络数组数据，和上面那个函数可以放在同一个class里 比如util
def getarray(str, aim):
    ans = []
    idx = str.find(aim) + len(aim)
    if idx != -1:
        idx2 = str.find(']', idx)
        while idx < idx2:
            tidx = str.find(',', idx)
            if tidx == -1 or tidx > idx2:
                tidx = idx2
            ans.append(float(str[idx:tidx]))
            idx = tidx + 1
    return ans

# 将intlist转化为字符串的函数
def iListTStr(l):
    lstr = [str(x) for x in l]
    return ', '.join(lstr)


# 将存储网络状态or Qoe的list更新(保留了最近长为30000的数据)
def updatelist(listlast, listnew, maxlen = 30000):
    listlast = listlast + listnew
    if len(listlast) > maxlen:
        listlast = listlast[len(listlast) - maxlen:]
    return listlast

# times合并为1s
def average(listt, len):
    j = 0
    res = []
    t = 0
    for i in listt:
        if j < len:
            j += 1
            t += i
        else:
            j = 0
            t = t * 1.0 / len
            res.append(t)
    if j != 0:
        res.append(t * 1.0 / j)
    return res

# 去掉列表中的0
def movezero(list):
    res = []
    for i in list:
        if i != 0:
            res.append(i)
    return res



# 将结果保存成图片可视化的函数
def saveResfig(addr, g_addr_qoe, g_addr_netcond, g_addr_slicetime):
    # with open (ipfrom, 'w') as f:
    #     f.write('qoe:\n')
    #     f.write(sutil.iListTStr(g_addr_qoe[ipfrom]))
    #     f.write('\nband:\n')
    #     f.write(sutil.iListTStr(g_addr_netcond[ipfrom][0]))
    #     f.write('\n')
    #     f.write(str(g_addr_slicetime[ipfrom]))
    pltres.initfig()
    strname = str(addr)
    pltres.pltlist(g_addr_qoe[addr], 'Time', 'QoE', strname + 'QoE')
    pltres.pltylin(g_addr_slicetime[addr], 'Start slice')
    pltres.finish(addr + 'QoE')

    pltres.initfig()
    pltres.pltlist(g_addr_netcond[addr][0], 'Time', 'Bandwidth', strname + 'Bandwidth')
    pltres.pltylin(g_addr_slicetime[addr], 'Start slice')
    pltres.finish(addr + 'Bandwidth')